/* eslint-disable no-undef */
import { Service } from "typedi";
import express, { Request, Response, NextFunction } from "express";
import JugadorController  from "../controller/jugador.controller";
import jwt from "jsonwebtoken";

const jugadorRouter = express.Router();

@Service()
export default class RouterJugador {
    public jugadorRouter = jugadorRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: JugadorController) {
        this.initialize();
    }

    verifyToken = (req: Request, res: Response, next: NextFunction) => {
        const token =
          req.body.token || req.query.token || req.headers["token"];
      
        if (!token) {
            return res.status(403).send("A token is required for authentication");
        }
        try {
            const decoded = jwt.verify(token, "SECRETA");
            req.body.user = decoded;
        } catch (err) {
            return res.status(401).send("Invalid Token");
        }
        return next();
    };

    initialize(){
        jugadorRouter.use(this.verifyToken);
        this.jugadorRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.jugadorRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.jugadorRouter.post("/", (req, res) => this.controller.create(req, res));
        this.jugadorRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.jugadorRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
    }
}