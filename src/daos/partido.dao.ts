import {Service} from "typedi";
import PartidoModel, { PartidoCreate, PartidoView } from "../models/partido";
import associations from "../models/associations";

@Service()
export default class PartidoDao {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly partidoModel: PartidoModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de partidoDaoCreada");
    }

    readAll(): Promise<PartidoView[] | null> {
        return this.partidoModel.PartidoModelInstance.findAll({
            attributes: ["lugar", "Fecha"],
            include: [
                {
                    association: this.associations.equipoPartido, 
                    attributes: ["nombre"]
                }
            ]
        });
    }
     
    read(Id: number): Promise<PartidoView | null> {
        return this.partidoModel.PartidoModelInstance.findByPk(Id);
    }

    create(Registro: PartidoCreate): Promise<PartidoView> {
        return this.partidoModel.PartidoModelInstance.create(Registro, {
            include: [ {association: this.associations.equipoPartido} ]});
    }

    update(Id: number, Registro: PartidoCreate): Promise<[number, PartidoView[]]> {
        return this.partidoModel.PartidoModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.partidoModel.PartidoModelInstance.destroy({where: {id: Id}});
    }
}