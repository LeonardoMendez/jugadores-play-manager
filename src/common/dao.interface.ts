/* eslint-disable no-unused-vars */
export default interface IDAO {
    read(id: number): any;
    create(t: any): any;
    update(p:number, t: any):any;
    delete(id: number):any;
// eslint-disable-next-line semi
}