/* eslint-disable no-undef */
import { Service } from "typedi";
import express from "express";
import LugarController  from "../controller/lugar.controller";

const lugarRouter = express.Router();

@Service()
export default class RouterLugar {
    public lugarRouter = lugarRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: LugarController) {
        this.initialize();
    }
    initialize(){
        this.lugarRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.lugarRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.lugarRouter.post("/", (req, res) => this.controller.create(req, res));
        this.lugarRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.lugarRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
    }
}