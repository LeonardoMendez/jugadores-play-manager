import { Service } from "typedi";
import { LugarCreate, LugarView } from "../models/lugar";
import LugarDao from "../daos/lugar.dao";
import axios from "axios";

@Service()
export default class LugarService {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly lugarDao: LugarDao){}

    create = (Registro: LugarCreate): Promise<LugarView> => {
        return this.lugarDao.create(Registro);
    };

    getById = (Id: number): Promise<LugarView | null> => {
        return this.lugarDao.read(Id);
    };

    deleteById = ( Id: number ): Promise<number> => {
        return this.lugarDao.delete(Id);
    };

    updateById = ( Id: number, Registro: LugarCreate ): Promise<[number, LugarView[]]> => {
        return this.lugarDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<LugarView[] | null> => {
        return this.lugarDao.readAll();
    };
}