/* eslint-disable no-undef */
import { Service } from "typedi";
import express from "express";
import EquipoController  from "../controller/equipo.controller";

const equipoRouter = express.Router();

@Service()
export default class RouterEquipo {
    public equipoRouter = equipoRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: EquipoController) {
        this.initialize();
    }
    initialize(){
        this.equipoRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.equipoRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.equipoRouter.post("/", (req, res) => this.controller.create(req, res));
        this.equipoRouter.post("/equipos", (req, res) => this.controller.createEquipo(req, res));
        this.equipoRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.equipoRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
    }
}