import { Service } from "typedi";
import { PosicionCreate, PosicionView } from "../models/posicion";
import PosicionDao from "../daos/posicion.dao";

@Service()
export default class PosicionService {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly posicionDao: PosicionDao){}

    create = (Registro: PosicionCreate): Promise<PosicionView> => {
        return this.posicionDao.create(Registro);
    };

    getById = (Id: number): Promise<PosicionView | null> => {
        return this.posicionDao.read(Id);
    };

    deleteById = ( Id: number ): Promise<number> => {
        return this.posicionDao.delete(Id);
    };

    updateById = ( Id: number, Registro: PosicionCreate ): Promise<[number, PosicionView[]]> => {
        return this.posicionDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<PosicionView[] | null> => {
        return this.posicionDao.readAll();
    };
}