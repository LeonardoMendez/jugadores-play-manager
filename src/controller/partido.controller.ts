/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import PartidoService from "../services/partido.service";
import { Service } from "typedi";

@Service()
export default class PartidoController{

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly partidoServices: PartidoService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.partidoServices.getAll());
    } 

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.partidoServices.getById(Number(req.params.id)));        
    }     
    
    async create(req: Request, res: Response) {
        let registro = req.body;
        res.json(await this.partidoServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        let Id = Number(req.params.pId);
        res.json(await this.partidoServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.partidoServices.deleteById(Number(req.params.pId)));
    }
}