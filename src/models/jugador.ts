import { Model, DataTypes, Optional } from "sequelize";
import SequelizeDAO from "../daos/sequelize";
import { Inject, Service } from "typedi";
//import PosicionModel from "./posicion";
//import EquipoModel from "./equipo";

interface JugadorAttributes {
    id: number
    nombre: string
}

interface JugadorInstance  extends Model<JugadorAttributes>, JugadorAttributes {}

export interface JugadorView {
    nombre: string
}

export interface JugadorCreate {
    id: number
    nombre: string
    posiciontipo_id: number
    equipo_id: number
}

@Service()
export default class JugadorModel {

    public JugadorModelInstance;
    //public posicionJugador: any;
    //public equipoJugador: any;

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO/*, private readonly posicionModel: PosicionModel, private readonly equipoModel: EquipoModel*/){
        this.JugadorModelInstance = this.sequelize.getSequelize().define<JugadorInstance>("Jugadores", {
            id: {
                primaryKey: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            nombre: {allowNull: false, type: DataTypes.STRING(60)}
        }, {
            tableName: "Jugadores",
            name: {
                singular: "Jugadores",
                plural: "Jugadores"
            },
            freezeTableName: true,
            timestamps: false
        });
        //this.posicionJugador = this.JugadorModelInstance.belongsTo(this.posicionModel.PosicionModelInstance, { foreignKey: {name: 'posiciontipo_id', allowNull: false}});
        //this.equipoJugador = this.JugadorModelInstance.belongsTo(this.equipoModel.EquipoModelInstance, { foreignKey: {name: 'equipo_id', allowNull: false}});
    }
}