import { Model, DataTypes, Optional } from "sequelize";
import SequelizeDAO from "../daos/sequelize";
import { Service } from "typedi";

interface LugarAttributes {
    id: number
    nombrelugar: string
    lat: number
    lng: number
}

interface LugarInstance  extends Model<LugarAttributes>, LugarAttributes {}

export interface LugarView {
    nombrelugar: string
    lat: number
    lng: number
}

export interface LugarCreate {
    id: number
    nombrelugar: string
    lat: number
    lng: number
}

@Service()
export default class LugarModel {
    
    public LugarModelInstance;
    

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO/*, private readonly jugadorModel: JugadorModel*/){
        this.LugarModelInstance = this.sequelize.getSequelize().define<LugarInstance>("Lugar", {
            id: {
                primaryKey: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            nombrelugar: { allowNull: false, type: DataTypes.STRING(40)},
            lat: {
                type: DataTypes.INTEGER,
            },
            lng: {
                type: DataTypes.INTEGER,
            },

        }, {
            tableName: "Lugar",
            name: {
                singular: "Lugar",
                plural: "Lugar"
            },
            freezeTableName: true,
            timestamps: false
        });
    }
}
