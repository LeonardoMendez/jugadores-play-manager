/* eslint-disable no-undef */
import "reflect-metadata";
import express from "express";
import routes from "./src/routes";
import { PUERTO } from "./src/utils/config-env";
import cors from "cors";

const app = express();

// Configurar cabeceras y cors
app.use(cors());
app.use(express.json());
app.use(routes);
app.listen(PUERTO, () => {
    console.log("Server started", PUERTO);
});