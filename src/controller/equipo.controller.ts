/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import EquipoService from "../services/equipo.service";
import { Service } from "typedi";

@Service()
export default class EquipoController{

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly equipoServices: EquipoService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.equipoServices.getAll());
    } 

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.equipoServices.getById(Number(req.params.id)));        
    }     
    
    async create(req: Request, res: Response) {
        let registro = req.body;
        res.json(await this.equipoServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        let Id = Number(req.params.pId);
        res.json(await this.equipoServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.equipoServices.deleteById(Number(req.params.pId)));
    }

    async createEquipo(req: Request, res: Response) {
        let registro = req.body;
        res.json(await this.equipoServices.createEquipo(registro));        
    }
}