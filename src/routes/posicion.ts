/* eslint-disable no-undef */
import { Service } from "typedi";
import express from "express";
import PosicionController  from "../controller/posicion.controller";

const posicionRouter = express.Router();

@Service()
export default class RouterPosicion {
    public posicionRouter = posicionRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: PosicionController) {
        this.initialize();
    }
    initialize(){
        this.posicionRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.posicionRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.posicionRouter.post("/", (req, res) => this.controller.create(req, res));
        this.posicionRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.posicionRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
    }
}