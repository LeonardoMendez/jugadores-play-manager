import {Service} from "typedi";
import LugarModel, { LugarCreate, LugarView } from "../models/lugar";
import associations from "../models/associations";

@Service()
export default class LugarDao {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly lugarModel: LugarModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de lugarDaoCreada");
    }

    readAll(): Promise<LugarView[] | null> {
        return this.lugarModel.LugarModelInstance.findAll({
            attributes: ["nombrelugar"]
        });
    }
     
    read(Id: number): Promise<LugarView | null> {
        return this.lugarModel.LugarModelInstance.findByPk(Id);
    }

    create(Registro: LugarCreate): Promise<LugarView> {
        return this.lugarModel.LugarModelInstance.create(Registro);
    }

    update(Id: number, Registro: LugarCreate): Promise<[number, LugarView[]]> {
        return this.lugarModel.LugarModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.lugarModel.LugarModelInstance.destroy({where: {id: Id}});
    }
}