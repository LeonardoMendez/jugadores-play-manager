/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Sequelize } from "sequelize";
import { Service } from "typedi";
import { DB_HOST, DB_PORT } from "../utils/config-env";

@Service()
export default class SequelizeDAO {
    define(arg0: string, arg1: { role: any; }) {
        throw new Error("Method not implemented.");
    }

    private dbSeq: Sequelize;

    constructor(){
        const db = "PLAYMANAGER";
        const username = "root";
        const password = "secret";
        const host = process.env.DB_HOST ? process.env.DB_HOST: "localhost";
        const port = process.env.DB_PORT ? process.env.DB_PORT: 33061;
        console.log("Host de la BD:", process.env.DB_HOST)
        console.log("Host", host)

        this.dbSeq = new Sequelize(db, username, password, {
            dialect: "mysql",
            port: Number(port),
            host: host,
            define: {
                freezeTableName: true
            }
        });
   
        try{
            this.dbSeq.authenticate().then(()=> console.log("Conectado..."));
        } catch(error){
            console.error("Error");
        }

        this.dbSeq.sync();
    }

    getSequelize(){
        return this.dbSeq;
    }

}