import { Service } from "typedi";
import { PartidoCreate, PartidoView } from "../models/partido";
import PartidoDao from "../daos/partido.dao";

@Service()
export default class PartidoService {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly partidoDao: PartidoDao){}

    create = (Registro: PartidoCreate): Promise<PartidoView> => {
        return this.partidoDao.create(Registro);
    };

    getById = (Id: number): Promise<PartidoView | null> => {
        return this.partidoDao.read(Id);
    };

    deleteById = ( Id: number ): Promise<number> => {
        return this.partidoDao.delete(Id);
    };

    updateById = ( Id: number, Registro: PartidoCreate ): Promise<[number, PartidoView[]]> => {
        return this.partidoDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<PartidoView[] | null> => {
        return this.partidoDao.readAll();
    };
}