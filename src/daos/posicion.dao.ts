import {Service} from "typedi";
import PosicionModel, { PosicionCreate, PosicionView } from "../models/posicion";
import associations from "../models/associations";

@Service()
export default class PosicionDao {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly posicionModel: PosicionModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de posicionDaoCreada");
    }

    readAll(): Promise<PosicionView[] | null> {
        return this.posicionModel.PosicionModelInstance.findAll({
            attributes: ["nombreposicion"],
            include: [
                {
                    association: this.associations.jugadorPosicion, 
                    attributes: ["nombre"]
                }
            ]
        });
    }
     
    read(Id: number): Promise<PosicionView | null> {
        return this.posicionModel.PosicionModelInstance.findByPk(Id);
    }

    create(Registro: PosicionCreate): Promise<PosicionView> {
        return this.posicionModel.PosicionModelInstance.create(Registro);
    }

    update(Id: number, Registro: PosicionCreate): Promise<[number, PosicionView[]]> {
        return this.posicionModel.PosicionModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.posicionModel.PosicionModelInstance.destroy({where: {id: Id}});
    }
}