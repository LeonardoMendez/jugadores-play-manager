import { Service } from "typedi"
import EquipoModel, { EquipoCreate } from "./equipo"
import JugadorModel, { JugadorCreate } from "./jugador"
import PosicionModel, { PosicionCreate } from "./posicion"

@Service()
export default class Bulk {
    constructor(private readonly posicionTipoModel: PosicionModel, private readonly equipoModel: EquipoModel, private readonly jugadorModel: JugadorModel) {
        this.populateTable()
    }

    populateTable() {
        posiciones.forEach( posicion => this.posicionTipoModel.PosicionModelInstance.create(posicion) )
        equipos.forEach( equipo => this.equipoModel.EquipoModelInstance.create(equipo) )
        jugadores.forEach( jugador => this.jugadorModel.JugadorModelInstance.create(jugador) )
    }
}


const jugadores: JugadorCreate[] = [
    { 
        id: 1,
        nombre: "jugador1",
        posiciontipo_id: 1,
        equipo_id: 1,
    },
    { 
        id: 2,
        nombre: "jugador2",
        posiciontipo_id: 2,
        equipo_id: 1,
    },
    { 
        id: 3,
        nombre: "jugador3",
        posiciontipo_id: 2,
        equipo_id: 1,
    },
    { 
        id: 4,
        nombre: "jugador4",
        posiciontipo_id: 4,
        equipo_id: 1,
    },
    { 
        id: 5,
        nombre: "jugador1",
        posiciontipo_id: 1,
        equipo_id: 2,
    },
    { 
        id: 6,
        nombre: "jugador2",
        posiciontipo_id: 2,
        equipo_id: 2,
    },
    { 
        id: 7,
        nombre: "jugador3",
        posiciontipo_id: 3,
        equipo_id: 2,
    },
    { 
        id: 8,
        nombre: "jugador4",
        posiciontipo_id: 4,
        equipo_id: 2,
    },
  ]

const equipos: EquipoCreate[] = [
    {
        id: 1,
        nombre: "equipo1",
    },
    {
        id: 2,
        nombre: "equipo2",
    },
    {
        id: 3,
        nombre: "equipo3",
    },
    {
        id: 4,
        nombre: "equipo4",
    }
]


const posiciones: PosicionCreate[] = [
    {
        id: 1,
        nombreposicion: "arquero",
    },
    {
        id: 2,
        nombreposicion: "defensa",
    },
    {
        id: 3,
        nombreposicion: "delantero",
    },
    {
        id: 4,
        nombreposicion: "mediocampo",
    }
]