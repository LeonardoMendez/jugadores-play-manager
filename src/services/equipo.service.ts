import { Service } from "typedi";
import { EquipoCreate, EquipoView } from "../models/equipo";
import EquipoDao from "../daos/equipo.dao";

@Service()
export default class EquipoService {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly equipoDao: EquipoDao){}

    create = (Registro: EquipoCreate): Promise<EquipoView> => {
        return this.equipoDao.create(Registro);
    };

    getById = (Id: number): Promise<EquipoView | null> => {
        return this.equipoDao.read(Id);
    };

    deleteById = ( Id: number ): Promise<number> => {
        return this.equipoDao.delete(Id);
    };

    updateById = ( Id: number, Registro: EquipoCreate ): Promise<[number, EquipoView[]]> => {
        return this.equipoDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<EquipoView[] | null> => {
        return this.equipoDao.readAll();
    };

    createEquipo = (Registro: EquipoCreate): Promise<EquipoView> => {
        return this.equipoDao.createEquipo(Registro);
    };
}