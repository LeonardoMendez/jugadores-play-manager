/* eslint-disable no-undef */
import { Service } from "typedi";
import express from "express";
import PartidoController  from "../controller/partido.controller";

const partidoRouter = express.Router();

@Service()
export default class RouterPartido {
    public partidoRouter = partidoRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: PartidoController) {
        this.initialize();
    }
    initialize(){
        this.partidoRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.partidoRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.partidoRouter.post("/", (req, res) => this.controller.create(req, res));
        this.partidoRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.partidoRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
    }
}