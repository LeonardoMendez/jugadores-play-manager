FROM node:fermium

WORKDIR /usr/src/app

COPY package.json .
RUN npm install
RUN npm install -g typescript
RUN npm install -g ts-node

COPY . .

EXPOSE 3005
CMD [ "npm","run" ,"serve" ]