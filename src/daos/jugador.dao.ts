import {Service} from "typedi";
import JugadorModel, { JugadorCreate, JugadorView } from "../models/jugador";
import associations from "../models/associations";

@Service()
export default class JugadorDao {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly jugadorModel: JugadorModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de jugadorDaoCreada");
    }

    readAll(): Promise<JugadorView[] | null> {
        return this.jugadorModel.JugadorModelInstance.findAll({
            attributes: ["nombre"],
            include: [
                {
                    association: this.associations.posicionJugador, 
                    attributes: ["nombreposicion"]
                }, {
                    association: this.associations.equipoJugador, 
                    attributes: ["nombre"]
                }
            ]
        });
    }
     
    read(Id: number): Promise<JugadorView | null> {
        return this.jugadorModel.JugadorModelInstance.findByPk(Id, {
            include: [
                {
                    association: this.associations.posicionJugador
                }
            ]
        });
    }

    create(Registro: JugadorCreate): Promise<JugadorView> {
        return this.jugadorModel.JugadorModelInstance.create(Registro);
    }

    update(Id: number, Registro: JugadorCreate): Promise<[number, JugadorView[]]> {
        return this.jugadorModel.JugadorModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.jugadorModel.JugadorModelInstance.destroy({where: {id: Id}});
    }
}