import { Model, DataTypes, Optional, Sequelize } from "sequelize";
import SequelizeDAO from "../daos/sequelize";
import { Service } from "typedi";

interface PartidoAttributes {
    id: number
    lugar: string
    fecha: Date
}

interface PartidoInstance  extends Model<PartidoAttributes>, PartidoAttributes {}

export interface PartidoView {
    lugar: string
    fecha: Date
}

export interface PartidoCreate {
    id: number
    lugar: string
    fecha: Date
    equipos:[]
}

@Service()
export default class PartidoModel {

    public PartidoModelInstance;

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO/*, private readonly equipoModel: EquipoModel*/){
        this.PartidoModelInstance = this.sequelize.getSequelize().define<PartidoInstance>("Partidos", {
            id: {
                primaryKey: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            lugar: {type: DataTypes.STRING(60)},
            fecha: {type: DataTypes.DATE}
        }, {
            tableName: "Partidos",
            name: {
                singular: "Partidos",
                plural: "Partidos"
            },
            freezeTableName: true,
            timestamps: false
        });
    }
}