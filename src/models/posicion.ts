import { Model, DataTypes, Optional } from "sequelize";
import SequelizeDAO from "../daos/sequelize";
import { Service } from "typedi";

interface PosicionAttributes {
    id: number
    nombreposicion: string
}

interface PosicionInstance  extends Model<PosicionAttributes>, PosicionAttributes {}

export interface PosicionView {
    nombreposicion: string
}

export interface PosicionCreate {
    id: number
    nombreposicion: string
}

@Service()
export default class PosicionModel {
    static PosicionModelInstance(PosicionModelInstance: any, arg1: { foreignKey: string; }) {
        throw new Error("Method not implemented.");
    }

    public PosicionModelInstance;
    

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO/*, private readonly jugadorModel: JugadorModel*/){
        this.PosicionModelInstance = this.sequelize.getSequelize().define<PosicionInstance>("PosicionTipo", {
            id: {
                primaryKey: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            nombreposicion: {allowNull: false, type: DataTypes.STRING(40)}
        }, {
            tableName: "PosicionTipo",
            name: {
                singular: "PosicionTipo",
                plural: "PosicionTipo"
            },
            freezeTableName: true,
            timestamps: false
        });
    }
}
