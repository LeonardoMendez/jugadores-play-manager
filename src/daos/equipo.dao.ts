import {Service} from "typedi";
import EquipoModel, { EquipoCreate, EquipoView } from "../models/equipo";
import associations from "../models/associations";

@Service()
export default class EquipoDao {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly equipoModel: EquipoModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de equipoDaoCreada");
    }

    readAll(): Promise<EquipoView[] | null> {
        return this.equipoModel.EquipoModelInstance.findAll();
        /*return this.equipoModel.EquipoModelInstance.findAll({
            attributes: ["nombre"],
            include: [
                {
                    association: this.associations.jugadorEquipo, 
                    attributes: ["nombre"]
                }
            ]
        });*/
    }
     
    read(Id: number): Promise<EquipoView | null> {
        return this.equipoModel.EquipoModelInstance.findByPk(Id);
    }

    create(Registro: EquipoCreate): Promise<EquipoView> {
        return this.equipoModel.EquipoModelInstance.create(Registro);
    }

    update(Id: number, Registro: EquipoCreate): Promise<[number, EquipoView[]]> {
        return this.equipoModel.EquipoModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.equipoModel.EquipoModelInstance.destroy({where: {id: Id}});
    }

    createEquipo(Registro: EquipoCreate): Promise<EquipoView> {
        return this.equipoModel.EquipoModelInstance.create(Registro, {
            include: [ {association: this.associations.jugadorEquipo} ]});
    }

    /*createEquipo(Registro: EquipoCreate): Promise<[EquipoView, boolean | null]> {
        return this.equipoModel.EquipoModelInstance.upsert(Registro, {
            include: [ {association: this.associations.jugadorEquipo} ]});
    }*/
}