import { Service } from "typedi";
import EquipoModel from "./equipo";
import JugadorModel from "./jugador";
import PartidoModel from "./partido";
import PosicionModel from "./posicion";
/*import SequelizeDAO from "../daos/sequelize";
import Bulk from "./bulk"*/

@Service()
export default class associations {

    public posicionJugador: any;
    public jugadorPosicion: any;
    public equipoJugador: any;
    public jugadorEquipo: any;
    public partidoEquipo: any;
    public equipoPartido: any;
  
    constructor (private readonly posicionModel: PosicionModel, 
                 private readonly equipoModel: EquipoModel,
                 private readonly jugadorModel: JugadorModel,
                 private readonly partidoModel: PartidoModel/*,
                 private readonly sequelize: SequelizeDAO,
                 private readonly bulk: Bulk*/){
        
        //Posición con jugador
        this.posicionJugador = this.jugadorModel.JugadorModelInstance.belongsTo(this.posicionModel.PosicionModelInstance, { foreignKey: {name: 'posiciontipo_id', allowNull: false}});
        this.jugadorPosicion = this.posicionModel.PosicionModelInstance.hasMany(this.jugadorModel.JugadorModelInstance, {sourceKey:"id", foreignKey: {name:"posiciontipo_id", allowNull: false}})

        //Equipo con jugador
        this.equipoJugador = this.jugadorModel.JugadorModelInstance.belongsTo(this.equipoModel.EquipoModelInstance, { foreignKey: {name: 'equipo_id', allowNull: false}});
        this.jugadorEquipo = this.equipoModel.EquipoModelInstance.hasMany(this.jugadorModel.JugadorModelInstance, {sourceKey:"id", foreignKey: {name:"equipo_id", allowNull: false}})
        
        //Equipo con partido
        this.equipoPartido = this.partidoModel.PartidoModelInstance.belongsToMany(this.equipoModel.EquipoModelInstance, { through: "EQUIPOPARTIDO" });
        this.partidoEquipo = this.equipoModel.EquipoModelInstance.belongsToMany(this.partidoModel.PartidoModelInstance, { through: "EQUIPOPARTIDO" });

        //this.sequelize.getSequelize().sync({force: true}).then(()=> {
            //this.bulk.populateTable()
        //})

    }
}