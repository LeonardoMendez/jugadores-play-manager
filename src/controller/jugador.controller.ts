/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import JugadorService from "../services/jugador.service";
import { Service } from "typedi";

@Service()
export default class JugadorController{

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly jugadorServices: JugadorService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.jugadorServices.getAll());
    } 

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.jugadorServices.getById(Number(req.params.id)));        
    }     
    
    async create(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        res.json(await this.jugadorServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        let Id = Number(req.params.pId);
        res.json(await this.jugadorServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.jugadorServices.deleteById(Number(req.params.pId)));
    }
}