/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import PosicionService from "../services/posicion.service";
import { Service } from "typedi";

@Service()
export default class PosicionController{

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly posicionServices: PosicionService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.posicionServices.getAll());
    } 

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.posicionServices.getById(Number(req.params.id)));        
    }     
    
    async create(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        res.json(await this.posicionServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        let Id = Number(req.params.pId);
        res.json(await this.posicionServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.posicionServices.deleteById(Number(req.params.pId)));
    }
}