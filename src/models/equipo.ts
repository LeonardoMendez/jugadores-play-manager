import { Model, DataTypes, Optional } from "sequelize";
import SequelizeDAO from "../daos/sequelize";
import { Service } from "typedi";

interface EquipoAttributes {
    id: number
    nombre: string
}

interface EquipoInstance  extends Model<EquipoAttributes>, EquipoAttributes {}

export interface EquipoView {
    nombre: string
}

export interface EquipoCreate {
    id: number
    nombre: string
}

@Service()
export default class EquipoModel {

    public EquipoModelInstance;

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO){
        this.EquipoModelInstance = this.sequelize.getSequelize().define<EquipoInstance>("Equipos", {
            id: {
                primaryKey: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            nombre: {allowNull: false, type: DataTypes.STRING(40)}
        }, {
            tableName: "Equipos",
            name: {
                singular: "Equipos",
                plural: "Equipos"
            },
            freezeTableName: true,
            timestamps: false
        });
    }
}