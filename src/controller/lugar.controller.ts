/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import LugarService from "../services/lugar.service";
import { Service } from "typedi";

@Service()
export default class LugarController {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly lugarServices: LugarService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.lugarServices.getAll());
    } 

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.lugarServices.getById(Number(req.params.id)));        
    }     
    
    async create(req: Request, res: Response) {
        let registro = req.body;
        res.json(await this.lugarServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body;
        let Id = Number(req.params.pId);
        res.json(await this.lugarServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.lugarServices.deleteById(Number(req.params.pId)));
    }
}