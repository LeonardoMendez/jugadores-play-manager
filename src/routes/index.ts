import express from "express";
import { Container } from "typedi";
import RouterPosicion from "./posicion";
import RouterJugador from "./jugador";
import RouterPartido from "./partido";
import RouterEquipo from "./equipo";
import RouterLugar from "./lugar";

const router = express.Router();
const posicionRoutes = Container.get<RouterPosicion>(RouterPosicion);
const jugadorRoutes = Container.get<RouterJugador>(RouterJugador);
const partidoRoutes = Container.get<RouterPartido>(RouterPartido);
const equipoRoutes = Container.get<RouterEquipo>(RouterEquipo);
const lugarRoutes = Container.get<RouterLugar>(RouterLugar);

router.use("/posicion", posicionRoutes.posicionRouter);
router.use("/jugador", jugadorRoutes.jugadorRouter);
router.use("/partido", partidoRoutes.partidoRouter);
router.use("/equipo", equipoRoutes.equipoRouter);
router.use("/lugar", lugarRoutes.lugarRouter);

export default router;