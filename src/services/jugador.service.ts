import { Service } from "typedi";
import JugadorDao from "../daos/jugador.dao";
import { JugadorCreate, JugadorView } from "../models/jugador";

@Service()
export default class PosicionService {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly jugadorDao: JugadorDao){}

    create = (Registro: JugadorCreate): Promise<JugadorView> => {
        return this.jugadorDao.create(Registro);
    };

    getById = (Id: number): Promise<JugadorView | null> => {
        return this.jugadorDao.read(Id);
    };

    deleteById = ( Id: number ): Promise<number> => {
        return this.jugadorDao.delete(Id);
    };

    updateById = ( Id: number, Registro: JugadorCreate ): Promise<[number, JugadorView[]]> => {
        return this.jugadorDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<JugadorView[] | null> => {
        return this.jugadorDao.readAll();
    };
}